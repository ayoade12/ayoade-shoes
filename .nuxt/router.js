import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _f11eb8f8 = () => interopDefault(import('../pages/Hub.vue' /* webpackChunkName: "pages/Hub" */))
const _754b5be5 = () => interopDefault(import('../pages/Men.vue' /* webpackChunkName: "pages/Men" */))
const _45844761 = () => interopDefault(import('../pages/Palm_m.vue' /* webpackChunkName: "pages/Palm_m" */))
const _4611326b = () => interopDefault(import('../pages/Palm_w.vue' /* webpackChunkName: "pages/Palm_w" */))
const _a0f8395c = () => interopDefault(import('../pages/Polish.vue' /* webpackChunkName: "pages/Polish" */))
const _a6aab044 = () => interopDefault(import('../pages/Sandal_m.vue' /* webpackChunkName: "pages/Sandal_m" */))
const _a590da30 = () => interopDefault(import('../pages/Sandal_w.vue' /* webpackChunkName: "pages/Sandal_w" */))
const _46b5106c = () => interopDefault(import('../pages/Shoe.vue' /* webpackChunkName: "pages/Shoe" */))
const _5c4797c2 = () => interopDefault(import('../pages/Shoe_on.vue' /* webpackChunkName: "pages/Shoe_on" */))
const _983bb554 = () => interopDefault(import('../pages/Sole.vue' /* webpackChunkName: "pages/Sole" */))
const _51bfa84d = () => interopDefault(import('../pages/Women.vue' /* webpackChunkName: "pages/Women" */))
const _7b231f41 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/Hub",
    component: _f11eb8f8,
    name: "Hub"
  }, {
    path: "/Men",
    component: _754b5be5,
    name: "Men"
  }, {
    path: "/Palm_m",
    component: _45844761,
    name: "Palm_m"
  }, {
    path: "/Palm_w",
    component: _4611326b,
    name: "Palm_w"
  }, {
    path: "/Polish",
    component: _a0f8395c,
    name: "Polish"
  }, {
    path: "/Sandal_m",
    component: _a6aab044,
    name: "Sandal_m"
  }, {
    path: "/Sandal_w",
    component: _a590da30,
    name: "Sandal_w"
  }, {
    path: "/Shoe",
    component: _46b5106c,
    name: "Shoe"
  }, {
    path: "/Shoe_on",
    component: _5c4797c2,
    name: "Shoe_on"
  }, {
    path: "/Sole",
    component: _983bb554,
    name: "Sole"
  }, {
    path: "/Women",
    component: _51bfa84d,
    name: "Women"
  }, {
    path: "/",
    component: _7b231f41,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
